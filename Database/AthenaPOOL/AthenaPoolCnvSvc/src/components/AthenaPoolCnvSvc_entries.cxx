#include "../AthenaPoolCnvSvc.h"
#include "../AthenaRootSerializeSvc.h"
#include "../AthenaAttributeListCnv.h"
#include "../CondAttrListCollCnv.h"
#include "../CondAttrListVecCnv.h"

DECLARE_COMPONENT( AthenaPoolCnvSvc )
DECLARE_COMPONENT( AthenaRootSerializeSvc )
DECLARE_CONVERTER( AthenaAttributeListCnv )
DECLARE_CONVERTER( CondAttrListCollCnv )
DECLARE_CONVERTER( CondAttrListVecCnv )

